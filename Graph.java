package cs303_LAB11;

public class Graph {
	
	private int size = 0;
	private GraphNode<Integer>[] table;
	private GraphNode<Integer>[] Q;
	private int ip = 0;
	private int fp = 0;
	
	@SuppressWarnings("unchecked")
	public Graph(int size) {
		
		this.size = size;
		table = new GraphNode[size];
	}
	
	public void insert(int node, int child) {
		
		if (table[node] == null) {
			//System.out.println(node);
			GraphNode<Integer> g = new GraphNode<Integer>(node, " ");
			if (table[child] == null) {
				
				GraphNode<Integer> c = new GraphNode<Integer>(child, " ");
				c.addNeighbor(g);
				g.addNeighbor(c);
				table[child] = c;
			}
			else {	
				//System.out.println(node);
				GraphNode<Integer> temp = table[child];
				temp.addNeighbor(g);
				g.addNeighbor(temp);
			}
			table[node] = g;
		}
		else {
	
			GraphNode<Integer> g = table[node];
			if (table[child] == null) {
				
				GraphNode<Integer> c = new GraphNode<Integer>(child, " ");
				c.addNeighbor(g);
				g.addNeighbor(c);
			}
			else {		
				GraphNode<Integer> temp = table[child];
				temp.addNeighbor(g);
				g.addNeighbor(temp);
			}
		}
	}
	
	public GraphNode<Integer> getNode(int pos){
		
		if (table[pos] != null) {
			return table[pos];
		}
		else {return null;}
	}
	
	public void breadthFirstSearch(Graph G, GraphNode<Integer> node) {
	
		int len = table.length;
		for (int i = 0; i < len; i++) {
			
			if (table[i] != null && table[i] != node) {
				table[i].setParent(null);
				table[i].setColor("white");
				table[i].setDistance(Integer.MAX_VALUE);
			}
		}
		node.setColor("grey");
		node.setDistance(0);
		node.setParent(null);
		
		buildQ(size);
		
		enqueue(node);
		while (getQueueFinalPointer() != getQueueInitialPointer()) {
			
			GraphNode<Integer> u = dequeue();
			GraphNode<Integer>[] adj = u.getNeighbor();
			for (GraphNode<Integer> i : adj) {
				
				if (i.getColor() == "white") {
					
					i.setColor("grey");
					i.setDistance(u.getDistance() + 1);
					i.setParent(u);
					enqueue(i);
				}
			}
			u.setColor("black");
		}
	}
	
	public void printPath(GraphNode<Integer> s, GraphNode<Integer> t) {
		
		if (s.equals(t)) {
			
			System.out.println(s.getValue());
		}
		else if (t.getParent() == null) {
			
			System.out.println("No path from " + s.getValue() + " to " + t.getValue());
		}
		else {
			printPath (s, t.getParent());
			System.out.println(t.getValue());
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public void buildQ(int size) {
		
		int len = table.length;
		Q = new GraphNode[len];
		ip = 0;
		fp = 0;
		//set two pointers 
	}
	
	public void enqueue(GraphNode<Integer> node) {
		
		int newFP = getQueueFinalPointer() + 1;
		if (newFP < Q.length) {
			
			Q[newFP-1] = node;
			setQueueFinalPointer();
		}
	}
	
	public GraphNode<Integer> dequeue() {
		
		int pos = getQueueInitialPointer();
		GraphNode<Integer> deq = Q[pos];
		Q[pos] = null;
		setQueueInitialPointer();
		return deq;
	}
	
	public int getQueueInitialPointer() {
		
		return ip;
		
	}
	
	public int getQueueFinalPointer() {
		
		return fp;
	}
	
	public void setQueueInitialPointer() {
		
		ip += 1;
	}
	
	public void setQueueFinalPointer() {
		
		fp += 1;
		
	}
}