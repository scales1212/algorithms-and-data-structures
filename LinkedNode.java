package cs303_LAB8;

public class LinkedNode{
	
	private LinkedNode p;
	private long v;
	private LinkedNode cL;
	private LinkedNode cR;
	private String data;
	
	public LinkedNode(long v, String data) {
		
		this.p = null;
		this.v = v;
		this.data = data;
		this.cL = null;
		this.cR = null;
	}
	
	public long getValue() {
		
		return v;
	}
	
	public String getData() {
		
		return data;
	}
	
	public LinkedNode getParent() {
		
		return p;
	}
	
	public LinkedNode getLeftChild() {
		
		return cL;
	}
	
	public LinkedNode getRightChild() {
		
		return cR;
	}
	
	public void setParent(LinkedNode i) {
		
		p = i;
	}
	
	public void setLeftChild(LinkedNode i) {
		
		cL = i;
	}
	
	public void setRightChild(LinkedNode i) {
		
		cR = i;
	}
}