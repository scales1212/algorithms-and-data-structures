package cs303_LAB11;

import java.util.Arrays;

public class GraphNode <T extends Comparable<T>>{
	
	private GraphNode <T> p;
	private T v;
	private GraphNode <T> child;
	private String data;
	private GraphNode<T>[] neighbors;
	private String color;
	private int d;
	
	
	public GraphNode() {
		this.p = null;
		this.v = null;
		this.child = null;
		this.color = "white";
		this.d = Integer.MAX_VALUE;
	}
	@SuppressWarnings("unchecked")
	public GraphNode(T v, String data) {
		
		this.p = null;
		this.v = v;
		this.data = data;
		this.child = null;
		this.neighbors = new GraphNode[0];
		this.color = "white";
		this.d = Integer.MAX_VALUE;
	}
	
	public T getValue() {
		
		return v;
	}
	
	public String getData() {
		
		return data;
	}
	
	public GraphNode<T> getParent() {
		
		return p;
	}
	
	public GraphNode<T> getChild() {
		
		return child;
	}
	
	public GraphNode<T>[] getNeighbor(){
		
		return neighbors;
	}
	
	public String getColor() {
		
		return color;
	}
	
	public int getDistance() {
		
		return d;
	}
	
	public void setParent(GraphNode<T> i) {
		
		p = i;
	}
	
	public void setChild(GraphNode<T> i) {
		
		child = i;
	}
	
	public void addNeighbor(GraphNode<T> i) {
		
		int len = neighbors.length;
		if (inNeighbor(i,len)) {
			neighbors = Arrays.copyOf(neighbors, len + 1);
			neighbors[len] = i;
		}
	}
	
	public boolean inNeighbor(GraphNode<T> t, int len) {
		
		boolean notInList = true;
		for (int i = 0; i < len; i ++) {
			
			if (neighbors[i] == t) {
				notInList = false;
			}
		}
		return notInList;
	}
	
	public void setDistance(int d) {
		
		this.d = d; 
	}
	
	public void setColor(String s) {
		
		this.color = s;
	}
	
	
}