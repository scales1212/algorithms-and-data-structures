package cs303_LAB9;

public class RBTree <T extends Comparable<T>> extends BinaryTree<T>{
	
	private RBNode<T> root;
	private RBNode<T> cursor;
	
	public RBTree() {
		
		super();
		this.root = null;
		this.cursor = null;
	}
	
	public void leftRotate(RBTree<T> T, RBNode<T> x) {
		
		RBNode<T> y = x.getRightChild();
		x.setRightChild(y.getLeftChild());
		
		if (y.getLeftChild() != null) {
			y.getLeftChild().setParent(x);
		}
		
		y.setParent(x.getParent());
		
		if (x.getParent() == null) {
			T.setRoot(y);
		}
		else if (x.equals(x.getParent().getLeftChild())) {
			x.getParent().setLeftChild(y);;
		}
		else {
			x.getParent().setRightChild(y);
		}
		y.setLeftChild(x);
		x.setParent(y);
		
	}
	
	public void rightRotate(RBTree<T> T, RBNode<T> x) {
		
		RBNode<T> y = x.getLeftChild();
		x.setLeftChild(y.getRightChild());
		
		if (y.getRightChild() != null) {
			y.getRightChild().setParent(x);
		}
		
		y.setParent(x.getParent());
		
		if (x.getParent() == null) {
			T.setRoot(y);
		}
		else if (x.equals(x.getParent().getRightChild())) {
			x.getParent().setRightChild(y);;
		}
		else {
			x.getParent().setLeftChild(y);
		}
		y.setRightChild(x);
		x.setParent(y);
		
	}
	
	public void RBInsert(RBTree<T> T, RBNode<T> z) {
		
		RBNode<T> y = null;
		RBNode<T> x = T.getRoot();
		
		while (x != null) {
			
			y = x;
			if (z.getValue().compareTo(x.getValue()) < 0) {
				x = x.getLeftChild();
			}
			else {x = x.getRightChild();}
		}
		z.setParent(y);
		if (y == null) {
			T.setRoot(z);
		}
		
		else if (z.getValue().compareTo(y.getValue()) < 0){
			y.setLeftChild(z);
		}
		else {y.setRightChild(z);}
		
		z.setLeftChild(null);
		z.setRightChild(null);
		z.setRed(true);
		RBInsertFix(T, z);
	}
	
	public void RBInsertFix(RBTree<T> T, RBNode<T> z) {
		if (z.getParent() != null) { 	
			while (z.getParent() != null && z.getParent().getRed()) {
				//System.out.println("fix");
				if (z.getParent().getParent() != null) {		
					//System.out.println("fix"); 
					if(z.getParent().equals(z.getParent().getParent().getLeftChild())) {
					
						RBNode <T> y = z.getParent().getParent().getRightChild();
						
						if (y != null && y.getRed() == true) {	
							
							z.getParent().setRed(false);
							y.setRed(false);
							z.getParent().getParent().setRed(true);
							z = z.getParent().getParent();
							
						}
						
						else {
							if (z.equals(z.getParent().getRightChild())) {
						
								z = z.getParent();
								leftRotate(T,z);
							}
							z.getParent().setRed(false);
							z.getParent().getParent().setRed(true);
							rightRotate(T, z.getParent().getParent());
						}
					}
					
					else {
						//System.out.println("fix");
						if (z.getParent().getParent() != null) {
							//System.out.println("fix");
							RBNode <T> y = z.getParent().getParent().getLeftChild();
							if (y != null && y.getRed() == true) {
								//System.out.println("fix");
								z.getParent().setRed(false);
								y.setRed(false);
								z.getParent().getParent().setRed(true);
								z = z.getParent().getParent();
							}
							else {
								if (z.equals(z.getParent().getLeftChild())) {
									//System.out.println("fix");
									z = z.getParent();
									rightRotate(T,z);
								}
								
								z.getParent().setRed(false);
								z.getParent().getParent().setRed(true);
								leftRotate(T, z.getParent().getParent());
								//System.out.println("fix");
							}
						}
					}
				}
			}
		}
		T.getRoot().setRed(false);
	}
	public void inorderWalk(RBNode<T> x) {
		
		if (x != null) {
			
			inorderWalk (x.getLeftChild());
			System.out.println(x.getValue());
			inorderWalk(x.getRightChild());
		}
	}

	public RBNode <T> search(RBNode <T> x , T k) {
		
		if (x == null || x.getValue().compareTo(k) == 0) {
			return x;
		}
		
		if (k.compareTo(x.getValue()) < 0) {
			return search(x.getLeftChild(), k);
		}
		else {return search(x.getRightChild(), k);}
	}

	
	public void setRoot(RBNode<T> r) {
		
		root = r;
	}
	
	public void setCursor (RBNode<T> c) {
		
		cursor = c;
	}
	
	public RBNode<T> getRoot() {
		
		return root;
	}
	
	public RBNode<T> getCursor() {
		
		return cursor;
	}
	
}