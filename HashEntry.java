package cs_LAB10;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


public class HashEntry {
      private long key;
      private String value;

      HashEntry(long key, String value) {
            this.key = key;
            this.value = value;
      }     

      public long getKey() {
            return key;
      }

      public String  getValue() {
            return value;
      }
      
      public void setValue(String val) {
            this.value = val;
      }
}
