package cs303_LAB6;

public class novelSort{
	
	public novelSort() {}
	
	public void sort(int[] lst) {
		
		int len = lst.length;
		
		for (int i = 0; i < len; i++) {
			
			findMinMax(lst, i, len);
			len -= 1;
		}
	}
	public void findMinMax(int[] lst, int p, int r) {
		
		int min = p;
		int max = r - 1;
		
		for (int i = min; i <= max; i++) {
			
			if (lst[i] > lst[max]) {
				int temp = lst[i];
				lst[i] = lst[max];
				lst[max] = temp;
			}
			if (lst[i] < lst[min]) {
				int temp = lst[i];
				lst[i] = lst[min];
				lst[min] = temp;
			}
		}
	}
}