package cs303_LAB2;

public class InsertionSort {
	
	
	public InsertionSort() {}
	
	public int[] sort(int[] lst) {
		
		int len = lst.length;
		
		for (int i = 1; i < len; i++) {
			
			int key = lst[i];
			int j = i - 1;
			while (j >= 0 && lst[j] > key) {
				lst[j+1] = lst[j];
				j -= 1;
			}
			lst[j+1] = key; 
		}
		return lst;
	}
}