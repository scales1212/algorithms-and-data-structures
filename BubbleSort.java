package cs303_LAB7; 

public class BubbleSort{
	
	public BubbleSort() {}
	
	public void sort(int[] A) {
		
		int len = A.length;
		
		for (int i = 0; i < len - 1; i++) {
			
			for (int j = len - 1; j > i; j--) {
				
				if (A[j] < A[j-1]) {
					int temp = A[j];
					A[j] = A[j-1];
					A[j - 1] = temp;
				}
			}
		}
	}
}