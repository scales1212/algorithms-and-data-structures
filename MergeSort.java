package cs303_LAB3;

import java.util.Arrays;

public class MergeSort {
	
	public MergeSort() {}
	
	public int[] sort(int[] lst, int p, int r) {
		
		if (p < r) {
			
			int q = (p + r)/2;
			sort(lst, p, q);
			sort(lst, q + 1, r);
			return merge(lst, p, q, r);
		}
		else {return lst;}
	}
	
	private int[] merge(int[] lst, int p, int q, int r) {
		
		int n1 = q - p + 1;
		int n2 = r - q;
		int[] L = {};
		int[] R = {};
		try {
			L = Arrays.copyOfRange(lst, p, n1);}
		catch (IllegalArgumentException e){
			L = Arrays.copyOf(lst, lst.length);
		}
		L = Arrays.copyOf(L, n1 + 1);
		try {
			R = Arrays.copyOfRange(lst, q, n2);}
		catch (IllegalArgumentException e) {
			R = Arrays.copyOf(lst, lst.length);
		}
		R = Arrays.copyOf(R, n2 + 1);
		System.out.println(L[0]);
		System.out.println(R[0]);
		L[n1] = Integer.MAX_VALUE;
		R[n2] = Integer.MAX_VALUE;
		
		int i = 0;
		int j = 0;
		for (int k = 1; k < r; k++) {
			
			if (L[i] <= R[j]){
				lst[k] = L[i];
				i += 1;
			}
			else {
				lst[k] = R[j];
				j += 1;
			}
		}
		return lst;
		}
		
	}
