package cs303_LAB7;

public class SelectionSort{
	
	public SelectionSort() {}
	
	public void sort(int[] A) {
		
		int len = A.length;
		
		for (int i = 0; i < len - 1; i++) {
			int min = i;
			
			for (int j = i + 1; j < len; j++) {
				
				if (A[j] < A[min]) {
					min = j;
				}
			}
			if (min != i) {
				int temp = A[min];
				A[min] = A[i];
				A[i] = temp;
			}
		}
	}
}