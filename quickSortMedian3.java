package cs303_LAB5;

public class quickSortMedian3 {
	
	public quickSortMedian3() {}
	
	public void sort (int[] A, int p, int r) {
		
		if (p < r) {
			int N = r - p;
			int m = median (A, p, p + (N/2), r-1);
			int t = A[r-1];
			A[r-1] = A[m];
			A[m] = t; 
			int q = partition (A, p, r);
			sort (A, p, q);
			sort (A, q + 1, r);
		}
	}
	public int partition(int[] A, int p, int r) {
		
		int x = A[r - 1];
		int i = p;
		for (int j = p; j < r -1; j++) {
			if (A[j] <= x) {
				i++;
				int temp = A[i-1];
				A[i-1] = A[j];
				A[j] = temp;
			}
		}
		int temp = A[i];
		A[i] = A[r - 1];
		A[r - 1] = temp;
		return i;
	}
	public int median(int[] A, int i, int j, int k) {
		
		if (A[i] >= A[j] && A[i] >= A[k]) {
			if (A[j] > A[k]) {return j;}
			else {return k;}
		}
		if (A[j] >= A[i] && A[j] >= A[k]) {
			if (A[i] > A[k]) {return i;}
			else {return k;}
		}
		else {
			if (A[i] > A[j]) {return i;}
			else {return j;}
		}
	}
	
}