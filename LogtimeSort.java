package cs303_LAB6;

import java.sql.Time;
import java.util.Arrays;

public class LogtimeSort{
	
	public LogtimeSort() {}
	
	public void sort(String[] lst) {
		
		//order list in alphabetical order only 
		int len = lst.length;
		for (int i = 0; i < len; i++) {
			
			alphSort(lst, i, len);
			len -= 1;
		}
		
		//create list of number of each city 
		int[] num = numCityList(lst);
		
		//keep alphabetical order but order times 
		for (int k = 0; k < num.length; k++) {
			int l = num[k];
			if (k == 0) {
				for (int a = 0; a < l; a++) {
					sortTime(lst, a, l);
					l -= 1;
				}
			}
			else {
				for (int a = num[k-1]; a < l; a++) {
					sortTime(lst, a, l);
					l -= 1;
				}
			}	
		}
	}
	/**
	 * Sorts the list of strings lst by alphabetical order, but does not sort by the time. The method is 
	 * searching for the largest and smallest values in a given sub list and orders the values appropriately
	 * @param lst
	 * @param p
	 * @param r
	 */
	public void alphSort(String[] lst, int p, int r) {
		
		int min = p;
		int max = r - 1;
		
		for (int i = min; i <= max; i++) {
			
			if (lst[i].compareTo(lst[max]) > 1) {
				String temp = lst[i];
				lst[i] = lst[max];
				lst[max] = temp;
			}
			if (lst[i].compareTo(lst[min]) < 1) {
				String temp = lst[i];
				lst[i] = lst[min];
				lst[min] = temp;
			}
		}
	}
	/**
	 * The method sorts the given string array lst by order of time while keeping the alphabetical order. 
	 * The method finds the largest and smallest values in a given sub list and sets those values, using a 
	 * time comparison
	 * @param lst
	 * @param p
	 * @param r
	 */
	public void sortTime (String[] lst, int p, int r) {
		
		int min = p;
		int max = r -1;
		
		for (int i = min; i <= max; i++) {
			int len = lst[i].length();
			String time = lst[i].substring(len - 8);
			Time t = Time.valueOf(time);
			
			int lenMin = lst[min].length();
			String timeMin = lst[min].substring(lenMin - 8);
			Time tMin = Time.valueOf(timeMin);
			
			int lenMax = lst[max].length();
			String timeMax = lst[max].substring(lenMax - 8);
			Time tMax = Time.valueOf(timeMax);
			
			if (t.before(tMin)) {
				String temp = lst[i];
				lst[i] = lst[min];
				lst[min] = temp;
			}
			if (t.after(tMax)) {
				String temp = lst[i];
				lst[i] = lst[max];
				lst[max] = temp;
			}
		}
	}
	/**
	 * method creates an int list of the total number of logs per city, so if chicago has 8 entries the value 
	 * representing the chicago entries would be 8. This method returns the int array
	 * @param lst
	 * @return
	 */
	public int[] numCityList (String[] lst) {
		
		int[] num = {};
		
		for (int j = 0; j < lst.length; j++) {
			if (j > 0) {
				String city = lst[j].substring(0, 5);
				if (lst[j-1].contains(city)) {}
				else {
					int length = num.length;
					num = Arrays.copyOf(num,length + 1);
					num[length] = j;
				}
			}
		}
		
		num = Arrays.copyOf(num, num.length + 1);
		num [num.length - 1] = lst.length;
		
		return num;
	}
	
}