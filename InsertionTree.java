package cs303_LAB8;

import java.util.ArrayList;

public class InsertionTree{
	
	private BinaryTree T;
	
	public InsertionTree() {}
	
	public void insert(BinaryTree T, LinkedNode z) {
		
		this.T = T; 
		LinkedNode y = null;
		
		LinkedNode x = T.getRoot();
			
		while (!x.equals(null)) {
				
			y = x;
			if (z.getValue() < x.getValue()) {
				x = x.getLeftChild();
			}
			else {
				x = x.getRightChild();
			}
		}
		z.setParent(y);
		
		if (y.equals(null)) {
			T.setRoot(z);;
		}
		
		else if (z.getValue() < y.getValue()) {
			
			y.setLeftChild(z);
		}
		
		else {y.setRightChild(z);}
		
	}
	
	public void inorderWalk(LinkedNode x) {
		
		if (!x.equals(null)) {
	
			inorderWalk (x.getLeftChild());
			System.out.println(x.getValue());
			inorderWalk(x.getRightChild());
		}
	}
		
}
