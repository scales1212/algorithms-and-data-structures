package cs303_LAB4;

public class HeapSort {
	
	private int heapSize = 0;
	public HeapSort () {}
	
	public void heap (int[] lst) {
	
		buildHeap(lst);
		for (int i = lst.length - 1; i > 1; i--) {
			int temp = lst[0];
			lst[0] = lst[i];
			lst[i] = temp;
			heapSize -= 1;
			maxHeap (lst, 0);	
		}
	}
	
	public void buildHeap (int[] lst) {
		
		heapSize = lst.length -1;
		for (int i = lst.length/2; i >= 0; i--) {
			maxHeap (lst, i);
		}	
	}
	
	public void maxHeap (int[] lst, int i){
		
		int l = 2*i;
		int r = 2*i + 1;
		int lrg = 0;
		if (l <= heapSize && lst[l] > lst[i]) {
			lrg = l;
		}
		else {lrg = i;}
		if (r <= heapSize && lst[r] > lst[lrg]) {
			lrg = r;
		}
		if (lrg != i) {
			int temp = lst[lrg];
			lst[lrg] = lst[i];
			lst[i] = temp;
			maxHeap (lst, lrg);
		}
	}
}