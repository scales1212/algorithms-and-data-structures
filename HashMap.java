package cs_LAB10;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


public class HashMap {
  private final static int TABLE_SIZE = 7*177650;

  HashEntry[] table;

  HashMap() {
       
	  table = new HashEntry[TABLE_SIZE];
  }

  public String get(long key) {
	  
	  int pos = (int) (key%TABLE_SIZE);
	  int count = 1;
	  Boolean t = false;
	  if (table[pos].getKey() == key) {
		  return table[pos].getValue();
	  }
	  while (!t) {
		 // System.out.println("pass");
		  pos = (int) (((7*count*pos) + 1)%TABLE_SIZE);
		  
		  if (table[pos] != null) {
			  
			  if (table[pos].getKey() != key) {
			     //System.out.println("pass");
				  count += 1;
			  }
			  else {return table[pos].getValue();}
		  }
		  if (count > 50) {return "Error: no associated key Value";}
	  }
	  return "Error: no associated key Value";
  }

  public void put(long key, String value) {
       
	  HashEntry h = new HashEntry(key, value);
	  Boolean t = false;
	  int pos = (int) (key%TABLE_SIZE);
	  if (table[pos] == null) {
		  table[pos] = h;
		  t = true;
	  }
	  else {
		  int count = 1;
		  while (!t) {
			  pos = ((7*count*pos) + 1)%TABLE_SIZE;
			  //System.out.println(key);
			  if (table[pos] != null) {
				  if (table[pos].getKey() == key) {
					  table[pos] = h;
					  t = true;
				  }
				  count += 1;
			  }
			  else {
				  table[pos] = h;
				  t = true;
				  }
		  }
	  }
  }

  public void linearProbe(long key, String value){
    
	  HashEntry h = new HashEntry(key, value);
	  Boolean t = false;
	  int i = 0;
	  while (!t) {
		  int pos = (int) ((key+i)%TABLE_SIZE);
		  if (table[pos] != null) {
			  if (table[pos].getKey() == key) {
			      table[pos] = h;
			      t = true;
			  }
			  i += 1;
		  }
		  else {
			  table[pos] = h;
			  t = true;
		  }
	  }
  }
  
  public String getLinear(long key) {
	  
	  int i = 0;
	  Boolean t = false;
	  while (!t) {
		  
		  int pos = (int) ((key+i)%TABLE_SIZE);
		  if (table[pos] != null) {
			  if (table[pos].getKey() == key) {
				  t = true;
				  return table[pos].getValue();
			  }
			  else {i += 1;}
				  
		  }
		  else {return "Nothing";}
	  }
	  return "Nothing";
  }

  public void quadraticProbe(long key, String value){
     
	  HashEntry h = new HashEntry(key, value);
	  Boolean t = false;
	  int i = 0;
	  while (!t) {
		  int pos = (int) ((key + Math.pow(i, 2))%TABLE_SIZE);
		  if (table[pos] != null) {
			  if (table[pos].getKey() == key) {
				  table[pos] = h;
				  t = true;
			  }
			  i += 1;
		  }
		  else {
			  table[pos] = h;
			  t = true;
		  }
	  }
  }
  
 public String getQuadratic(long key) {
	  
	  int i = 0;
	  Boolean t = false;
	  while (!t) {
		  
		  int pos = (int) ((key+ Math.pow(i, 2))%TABLE_SIZE);
		  if (table[pos] != null) {
			  if (table[pos].getKey() == key) {
				  t = true;
				  return table[pos].getValue();
			  }
			  else {i += 1;}
				  
		  }
	  }
	  return "Nothing";
  }

}
