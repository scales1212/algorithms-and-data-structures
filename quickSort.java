package cs303_LAB5;

public class quickSort{
	
	public quickSort() {}
	
	public void sort (int[] A, int p, int r) {
		
		if (p < r) {
			
			int q = partition (A, p, r);
			sort (A, p, q);
			sort (A, q + 1, r);
		}
	}
	public int partition(int[] A, int p, int r) {
		
		int x = A[r - 1];
		int i = p;
		for (int j = p; j < r -1; j++) {
			if (A[j] <= x) {
				i++;
				int temp = A[i-1];
				A[i-1] = A[j];
				A[j] = temp;
			}
		}
		int temp = A[i];
		A[i] = A[r - 1];
		A[r - 1] = temp;
		return i;
	}
}