package cs303_LAB8;

public class BinaryTree{
	
	private LinkedNode root;
	private LinkedNode cursor;
	public BinaryTree() {
		
		root = null;
		cursor = null;
	}
	
public void insert(BinaryTree T, LinkedNode z) {
		
		LinkedNode y = null;
		
		LinkedNode x = T.getRoot();
			
		while (x != null) {
				
			y = x;
			long t = z.getValue();
			long i = x.getValue();
			//System.out.println(t);
			//System.out.println(i);
			if (t < i) {
				x = x.getLeftChild();
			}
			else {
				x = x.getRightChild();
			}
		}
		z.setParent(y);
		
		if (y == null) {
			T.setRoot(z);
		
		}
		
		else if (z.getValue() < y.getValue()) {
			
			y.setLeftChild(z);
			
		}
		
		else {y.setRightChild(z); 
	
		}
		
}
	
	public void inorderWalk(LinkedNode x) {
		
		if (x != null) {
	
			inorderWalk (x.getLeftChild());
			System.out.println(x.getValue());
			inorderWalk(x.getRightChild());
		}
	}
	
	public LinkedNode search(LinkedNode x, double k) {
		
		if (x == null || x.getValue() == k) {
			return x;
		}
		
		if (k < x.getValue()) {
			return search(x.getLeftChild(), k);
		}
		else {return search(x.getRightChild(), k);}
	}

	
	public void setRoot(LinkedNode r) {
		
		root = r;
	}
	
	public void setCursor (LinkedNode c) {
		
		cursor = c;
	}
	
	public LinkedNode getRoot() {
		
		return root;
	}
	
	public LinkedNode getCursor() {
		
		return cursor;
	}
	
}