package cs303_LAB9;

public class RBNode<T extends Comparable<T>> extends LinkedNode <T> {
	
	private T v;
	private String data;
	private boolean red = true;
	
	private RBNode<T> p;
	private RBNode<T> cL;
	private RBNode<T> cR;
	
	public RBNode() {
		super();
		
	}
	
	public RBNode(T v, String data) {
		
		super(v, data);
		this.v = v;
		this.data = data;
	}
	
	public boolean getRed() {
		
		return red;
	}
	
	public void setRed(boolean t) {
		
		red = t;
	}
	

	public T getValue() {
		
		return v;
	}
	
	public String getData() {
		
		return data;
	}
	
	public RBNode<T> getParent() {
		
		return p;
	}
	
	public RBNode<T> getLeftChild() {
		
		return cL;
	}
	
	public RBNode<T> getRightChild() {
		
		return cR;
	}
	
	public void setParent(RBNode<T> i) {
		
		p = i;
	}
	
	public void setLeftChild(RBNode<T> i) {
		
		cL = i;
	}
	
	public void setRightChild(RBNode<T> i) {
		
		cR = i;
	}
	
}